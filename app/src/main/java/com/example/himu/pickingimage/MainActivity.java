package com.example.himu.pickingimage;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import static com.example.himu.pickingimage.DbBitmapUtility.getImage;

public class MainActivity extends AppCompatActivity {


    private static int RESULT_LOAD = 1;
    String img_Decodable_Str;
    DbBitmapUtility dbBitmapUtility;
    String bits;
    Bitmap yourSelectedImage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dbBitmapUtility = new DbBitmapUtility();
    }


    public void Button (View v){

        //******Create intenet to open image application like gallery and google photos
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the intent
        startActivityForResult(galleryIntent, RESULT_LOAD);

    }

    public void showButton(View v){

        Bitmap bitmap = dbBitmapUtility.StringToBitMap(bits);
        ImageView img = (ImageView) findViewById(R.id.imageView);
        //set the image in imageview after decoding the string


        byte[] inserted = dbBitmapUtility.getBytes(bitmap);
        Log.i("Log",inserted.toString());
        Bitmap bitmap1 = dbBitmapUtility.getImage(inserted);

        img.setImageBitmap(bitmap1);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try{
            //when an image is picked
            if (requestCode == RESULT_LOAD && resultCode ==  RESULT_OK && null != data){

                //get the image from data

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA };

                //get the cursor

                Cursor cursor = getContentResolver().query(selectedImage,filePathColumn, null, null, null);

                // Move to first row

                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                img_Decodable_Str = cursor.getString(columnIndex);
                yourSelectedImage = BitmapFactory.decodeFile(img_Decodable_Str);

                cursor.close();



                 bits=dbBitmapUtility.BitMapToString(yourSelectedImage);
                Log.i("Log",bits);


            }
            else {
                Toast.makeText(getApplicationContext(),"Hey pick your image first",Toast.LENGTH_LONG).show();
            }
        } catch (Exception e){
            Toast.makeText(getApplicationContext(),"Somethig went embarassing",Toast.LENGTH_LONG).show();
        }
    }
}
